#!/usr/bin/env make

.PHONY:	install
ALL: 	install

PREFIX ?= /usr/local
GROUP ?= users
SHELL:=/bin/bash

build:
	pod2man -c mr mr > mr.1
	pod2man -c webcheckout webcheckout > webcheckout.1

install: build
	sudo install -m 755 -g ${GROUP} mr ${PREFIX}/bin/mr
	sudo install -m 755 -g ${GROUP} webcheckout ${PREFIX}/bin/webcheckout
	sudo install -m 755 -g ${GROUP} mr-recache ${PREFIX}/bin/mr-recache
	sudo install -m 755 -g ${GROUP} mrdir ${PREFIX}/bin/mrdir
	#install -m 755 -g ${GROUP} zsh/mrcd ${HOME}/.zsh/functions/mrcd
	#install -m 644 -g ${GROUP} zsh/_mr_repositories	${HOME}/.zsh/functions/_mr_repositories
	#install -m 775 -g ${GROUP} -d ${PREFIX}/share/man/man1
	install -m 644 -g ${GROUP} mr.1	${PREFIX}/share/man/man1/mr.1
	install -m 644 -g ${GROUP} webcheckout.1 ${PREFIX}/share/man/man1/webcheckout.1

clean:
	rm ${PREFIX}/bin/mr
	rm ${PREFIX}/bin/webcheckout
	rm ${PREFIX}/bin/mr-recache
	rm ${PREFIX}/bin/mrdir
	#rm ${HOME}/.zsh/functions/mrcd
	#rm ${HOME}/.zsh/functions/_mr_repositories
	rm ${PREFIX}/share/man/man1/mr.1
	rm ${PREFIX}/share/man/man1/webcheckout.1

test:
	(echo "[.]"; echo "checkout=") > mrconfig.tmp
	./mr --trust-all -c mrconfig.tmp ed | grep -q "horse"
	rm -f mrconfig.tmp
